<h1 align="center">ASCII Path Finder</h1>
<p align="center">
  <a href="#about">About</a> &#xa0; | &#xa0;
  <a href="#technologies">Technologies</a> &#xa0; | &#xa0;
  <a href="#requirements">Requirements</a> &#xa0; | &#xa0;
  <a href="#starting">Starting</a> &#xa0; | &#xa0;
  <a href="#building">Building</a> &#xa0; | &#xa0;
  <a href="#testing">Testing</a> &#xa0; | &#xa0;
  <a href="#author">Author</a> &#xa0; | &#xa0;
</p>

<br>

## About

The ASCII Path Finder project solves the problem of finding the path and letters along the path in an ASCII map file.
For Example, given the file below as input:

<pre>
  @---A---+
          |
  x-B-+   C
      |   |
      +---+
</pre>

the script should return following output:

<pre>
Letters ACB
Path as characters @---A---+|C|+---+|+-B-x
</pre>

## Technologies

The following tools were used in this project:

- [Node.js](https://nodejs.org/en/)
- [TypeScript](https://www.typescriptlang.org/)

## Requirements

Before starting you need to have [Git](https://git-scm.com) and [Node](https://nodejs.org/en/) v12.0.0+ installed.

## Starting

```bash
# Clone this project
$ git clone git@gitlab.com:spopara/ascii-path-finder.git

# Access
$ cd ascii-path-finder

# Install dependencies
$ npm install

# Run the project
$ npm start -- -f "./ascii-maps/valid/basic.txt"
```

## Building

```bash
# Build the project
$ npm run build
```

## Testing

```bash
# Run unit tests
$ npm run test:unit

# Run acceptance tests
$ npm run test:acceptance

# Run all tests
$ npm run test
```

## Author

Made by Srđan Popara
<pre>
´´´´´´´¶¶¶¶´´´´´´´´´´´´´´´´´´
´´´´´´¶¶´´´´¶¶¶¶¶´´¶¶¶¶´¶¶¶¶´´
´´´´´´¶´´´´´´´´´´¶¶¶¶´¶¶´´´´¶´
´´´´´´¶´´´´´´´´´´¶´¶¶¶¶¶¶´´´¶´
´´´´´¶´´´´´´´´´´¶¶¶¶¶´´´¶¶¶¶¶´
´´´´¶´´´´´´´´´´´´´´´´¶¶¶¶¶¶¶¶´
´´´¶´´´´´´´´´´´´´´´´´´´¶¶¶¶¶´´
´¶¶¶´´´´´¶´´´´´´´´´´´´´´´´´¶´´
´´´¶´´´´¶¶´´´´´´´´´´´´´´´´´¶´´
´´´¶¶´´´´´´´´´´´´´´´´¶¶´´´´¶´´
´´¶¶¶´´´´´´´´´¶¶¶´´´´¶¶´´´¶¶´´
´´´´´¶¶´´´´´´´´´´´´´´´´´´¶¶¶´´
´´´´´´´¶¶¶´´´´´´´´´´´´´¶¶¶´´´´
´´´¶¶¶¶¶´¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶´´´´´´
´´´¶´´´´¶¶¶¶¶´´´´¶¶¶¶´´´¶´´´´´
´´´¶´´´´¶¶¶´¶¶¶¶¶¶¶¶´´´¶¶¶´´´´
´´´¶¶¶¶¶¶¶¶¶¶¶¶¶´´¶¶¶¶¶´´´¶¶´´
´´¶´´´´´´¶¶¶¶¶¶¶¶¶¶¶´´´´´´´¶´´
´¶´´´´´´´´´¶¶¶¶¶¶¶¶´´´´´´´´¶´´
´´¶´´´´´´´´¶¶¶¶¶¶¶¶´´´´´´´´¶´´
´´¶¶´´´´´´´¶¶´´´´¶¶´´´´´´¶¶´´´
´´´´¶¶¶¶¶¶¶´´´´´´´´¶¶¶¶¶¶´´´´´
</pre>