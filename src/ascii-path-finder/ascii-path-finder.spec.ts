import { ERROR_MESSAGE, SYMBOL } from "../common/enums";
import { AsciiElement } from "../common/types";
import { AsciiPathFinder } from "./ascii-path-finder";

const expect = require("chai").expect;

describe("Unit Test (Ascii Path Finder)", () => {
  /*
  @--S
     O
  -xS+
  */
  const validAsciiMatrix: AsciiElement[][] = [
    [
      { value: SYMBOL.START, processed: false },
      { value: SYMBOL.LINE_HORIZONTAL, processed: false },
      { value: SYMBOL.LINE_HORIZONTAL, processed: false },
      { value: "S", processed: false },
    ],
    [
      { value: " ", processed: false },
      { value: " ", processed: false },
      { value: " ", processed: false },
      { value: "O", processed: false },
    ],
    [
      { value: SYMBOL.LINE_HORIZONTAL, processed: false },
      { value: SYMBOL.END, processed: false },
      { value: "S", processed: false },
      { value: SYMBOL.CORNER, processed: false },
    ],
  ];
  const correctLetters = ["S", "O", "S"];
  const correctPath = [
    SYMBOL.START,
    SYMBOL.LINE_HORIZONTAL,
    SYMBOL.LINE_HORIZONTAL,
    "S",
    "O",
    SYMBOL.CORNER,
    "S",
    SYMBOL.END,
  ];

  it("Should find correct letters", () => {
    let asciiPathFinder: AsciiPathFinder = new AsciiPathFinder(validAsciiMatrix);
    expect(asciiPathFinder.getLetters()).to.deep.equal(correctLetters);
  });

  it("Should find correct path", () => {
    let asciiPathFinder: AsciiPathFinder = new AsciiPathFinder(validAsciiMatrix);
    expect(asciiPathFinder.getPath()).to.deep.equal(correctPath);
  });

  it("Should throw exception if multiple starts", () => {
    const invalidAsciiMatrix = [
      [
        { value: SYMBOL.START, processed: false },
        { value: SYMBOL.LINE_HORIZONTAL, processed: false },
        { value: SYMBOL.START, processed: false },
        { value: SYMBOL.END, processed: false },
      ],
    ];
    expect(() => {
      new AsciiPathFinder(invalidAsciiMatrix);
    }).to.throw(ERROR_MESSAGE.MULTIPLE_START);
  });

  it("Should throw exception if multiple ends", () => {
    const invalidAsciiMatrix = [
      [
        { value: SYMBOL.START, processed: false },
        { value: SYMBOL.END, processed: false },
        { value: SYMBOL.LINE_HORIZONTAL, processed: false },
        { value: SYMBOL.END, processed: false },
      ],
    ];
    expect(() => {
      new AsciiPathFinder(invalidAsciiMatrix);
    }).to.throw(ERROR_MESSAGE.MULTIPLE_END);
  });

  it("Should throw exception if path is broken", () => {
    const invalidAsciiMatrix = [
      [
        { value: SYMBOL.START, processed: false },
        { value: SYMBOL.LINE_HORIZONTAL, processed: false },
        { value: " ", processed: false },
        { value: SYMBOL.END, processed: false },
      ],
    ];
    expect(() => {
      new AsciiPathFinder(invalidAsciiMatrix);
    }).to.throw(ERROR_MESSAGE.BROKEN_PATH);
  });

  it("Should throw exception if there is an invalid character", () => {
    const invalidAsciiMatrix = [
      [
        { value: SYMBOL.START, processed: false },
        { value: SYMBOL.LINE_HORIZONTAL, processed: false },
        { value: SYMBOL.END, processed: false },
        { value: "%", processed: false },
      ],
    ];
    expect(() => {
      new AsciiPathFinder(invalidAsciiMatrix);
    }).to.throw(ERROR_MESSAGE.INVALID_CHARACTER);
  });

  it("Should throw exception if there is no start symbol", () => {
    const invalidAsciiMatrix = [
      [
        { value: SYMBOL.END, processed: false },
        { value: SYMBOL.LINE_HORIZONTAL, processed: false },
      ],
    ];
    expect(() => {
      new AsciiPathFinder(invalidAsciiMatrix);
    }).to.throw(ERROR_MESSAGE.NO_START);
  });

  it("Should throw exception if there is no end symbol", () => {
    const invalidAsciiMatrix = [
      [
        { value: SYMBOL.START, processed: false },
        { value: SYMBOL.LINE_HORIZONTAL, processed: false },
      ],
    ];
    expect(() => {
      new AsciiPathFinder(invalidAsciiMatrix);
    }).to.throw(ERROR_MESSAGE.NO_END);
  });

  it("Should throw exception if there is a T-fork", () => {
    const invalidAsciiMatrix = [
      [
        { value: SYMBOL.LINE_HORIZONTAL, processed: false },
        { value: SYMBOL.START, processed: false },
        { value: SYMBOL.LINE_HORIZONTAL, processed: false },
        { value: SYMBOL.END, processed: false },
      ],
    ];
    expect(() => {
      new AsciiPathFinder(invalidAsciiMatrix);
    }).to.throw(ERROR_MESSAGE.T_FORK);
  });
});
