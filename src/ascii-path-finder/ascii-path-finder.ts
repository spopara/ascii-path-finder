import { AsciiPathFinderUtils } from "../ascii-path-finder-utils/ascii-path-finder-utils";
import { DIRECTION, ERROR_MESSAGE, SYMBOL } from "../common/enums";
import { AsciiElement, Position, Step } from "../common/types";

/**
 * Travels through an ASCII matrix from the start symbol
 * to the end symbol and collects letters along the way.
 */
export class AsciiPathFinder {
  private asciiMatrix: AsciiElement[][] = [];
  private currentPosition: Position = { x: 0, y: 0 };
  private currentDirection: DIRECTION | null = null;
  private path: string[] = [];
  private letters: string[] = [];

  constructor(asciiMatrix: AsciiElement[][]) {
    this.asciiMatrix = asciiMatrix;
    this.travelPath();
  }

  getLetters(): string[] {
    return this.letters;
  }

  getPath(): string[] {
    return this.path;
  }

  /**
   * Returns the appropriate next step that can be taken from the current position.
   * Step in reverse directions is exculded.
   * Step in the same direction is prioritized.
   *
   * @param currentPosition - current position in the ASCII matrix.
   * @throws {Erorr} - if a T-fork is detected.
   * @throws {Erorr} - if the path is broken.
   * @returns the next step.
   */
  private findNextStep(): Step {
    let progressiveDirections: DIRECTION[] = AsciiPathFinderUtils.getProgressiveDirections(this.currentDirection);

    const stepCandidates: Step[] = [];
    progressiveDirections.forEach((direction) => {
      const stepCandidate = this.getStepCandidate(direction);
      if (stepCandidate) {
        stepCandidates.push(stepCandidate);
      }
    });

    if (!stepCandidates.length) {
      throw new Error(ERROR_MESSAGE.BROKEN_PATH);
    }

    if (AsciiPathFinderUtils.isTFork(stepCandidates.map((step) => step.direction))) {
      throw new Error(ERROR_MESSAGE.T_FORK);
    }

    return stepCandidates[0];
  }

  /**
   * Returns the position of the start and end symbol in the ASCII matrix.
   *
   * @param asciiMatrix - the ASCII matrix to process.
   * @throws {Erorr} - if the ASCII matrix has multiple start symbols.
   * @throws {Erorr} - if the ASCII matrix has multiple end symbols.
   * @throws {Erorr} - if the ASCII matrix has no start symbol.
   * @throws {Erorr} - if the ASCII matrix has no end symbol.
   * @returns start and end symbol positions.
   */
  private findStartEndSymbolPositions(asciiMatrix: AsciiElement[][]): {
    startPosition: Position;
    endPosition: Position;
  } {
    let startPosition: Position | null = null;
    let endPosition: Position | null = null;
    asciiMatrix.forEach((row, x) => {
      row.forEach((asciiElement, y) => {
        switch (asciiElement.value) {
          case SYMBOL.START:
            if (startPosition) {
              throw new Error(ERROR_MESSAGE.MULTIPLE_START);
            }
            startPosition = { x, y };
            break;

          case SYMBOL.END:
            if (endPosition) {
              throw new Error(ERROR_MESSAGE.MULTIPLE_END);
            }
            endPosition = { x, y };
            break;

          default:
            if (
              !AsciiPathFinderUtils.isEmptySpace(asciiElement.value) &&
              !AsciiPathFinderUtils.isValidSymbol(asciiElement.value) &&
              !AsciiPathFinderUtils.isValidCapitalLetter(asciiElement.value)
            ) {
              throw new Error(ERROR_MESSAGE.INVALID_CHARACTER);
            }
        }
      });
    });

    if (!startPosition) {
      throw new Error(ERROR_MESSAGE.NO_START);
    }

    if (!endPosition) {
      throw new Error(ERROR_MESSAGE.NO_END);
    }

    return { startPosition, endPosition };
  }

  /**
   * Returns the step if it can be taken in the given direction.
   *
   * @param direction - direction in which to take the step.
   * @returns the step if it can be taken, null otherwise.
   */
  private getStepCandidate(direction: DIRECTION): Step | null {
    // must be within the matrix boundaries
    const nextPosition = AsciiPathFinderUtils.getNextPosition(direction, this.currentPosition);
    if (!AsciiPathFinderUtils.isWithinMatrixBoundaries(nextPosition, this.asciiMatrix)) {
      return null;
    }

    // must be a valid symbol or letter
    const nextAsciiElement = AsciiPathFinderUtils.getAsciiElement(nextPosition, this.asciiMatrix);
    if (
      !AsciiPathFinderUtils.isValidSymbol(nextAsciiElement.value) &&
      !AsciiPathFinderUtils.isValidCapitalLetter(nextAsciiElement.value)
    ) {
      return null;
    }

    // must turn on corners
    const currentAsciiElement = AsciiPathFinderUtils.getAsciiElement(this.currentPosition, this.asciiMatrix);
    if (AsciiPathFinderUtils.isCornerSymbol(currentAsciiElement.value)) {
      if (!AsciiPathFinderUtils.isChangingDirection(direction, this.currentDirection)) {
        return null;
      }
    }

    return { direction, asciiElement: nextAsciiElement, position: nextPosition };
  }

  /**
   * Travels the path and collects letters and path characters along the way.
   */
  private travelPath(): void {
    this.currentPosition = this.findStartEndSymbolPositions(this.asciiMatrix).startPosition;
    let currentAsciiElement: AsciiElement = AsciiPathFinderUtils.getAsciiElement(
      this.currentPosition,
      this.asciiMatrix
    );
    this.path.push(currentAsciiElement.value);
    while (currentAsciiElement?.value !== SYMBOL.END) {
      const nextStep = this.findNextStep();
      currentAsciiElement = nextStep.asciiElement;
      this.currentPosition = nextStep.position;
      this.currentDirection = nextStep.direction;
      this.path.push(currentAsciiElement.value);
      if (!currentAsciiElement.processed && AsciiPathFinderUtils.isValidCapitalLetter(currentAsciiElement.value)) {
        this.letters.push(currentAsciiElement.value);
        currentAsciiElement.processed = true;
      }
    }
  }
}
