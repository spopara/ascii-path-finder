import { DIRECTION, SYMBOL } from "../common/enums";
import { AsciiElement } from "../common/types";
import { AsciiPathFinderUtils } from "./ascii-path-finder-utils";

const expect = require("chai").expect;

describe("Unit Test (Ascii Path Finder Utils)", () => {
  const asciiMatrix: AsciiElement[][] = [
    [
      { value: SYMBOL.START, processed: false },
      { value: SYMBOL.LINE_HORIZONTAL, processed: false },
    ],
    [
      { value: SYMBOL.LINE_VERTICAL, processed: false },
      { value: SYMBOL.END, processed: false },
    ],
  ];

  describe("getAsciiElement", () => {
    it("Should return correct element from matrix", () => {
      const asciiElement = AsciiPathFinderUtils.getAsciiElement({ x: 1, y: 1 }, asciiMatrix);
      expect(asciiElement).to.deep.equal({ value: SYMBOL.END, processed: false });
    });
  });

  describe("getNextPosition", () => {
    it("Should return correct position in direction DOWN", () => {
      const position = AsciiPathFinderUtils.getNextPosition(DIRECTION.DOWN, { x: 1, y: 1 });
      expect(position).to.deep.equal({ x: 2, y: 1 });
    });

    it("Should return correct position in direction UP", () => {
      const position = AsciiPathFinderUtils.getNextPosition(DIRECTION.UP, { x: 1, y: 1 });
      expect(position).to.deep.equal({ x: 0, y: 1 });
    });

    it("Should return correct position in direction LEFT", () => {
      const position = AsciiPathFinderUtils.getNextPosition(DIRECTION.LEFT, { x: 1, y: 1 });
      expect(position).to.deep.equal({ x: 1, y: 0 });
    });

    it("Should return correct position in direction RIGHT", () => {
      const position = AsciiPathFinderUtils.getNextPosition(DIRECTION.RIGHT, { x: 1, y: 1 });
      expect(position).to.deep.equal({ x: 1, y: 2 });
    });
  });

  describe("getProgressiveDirections", () => {
    it("Should return LEFT, RIGHT, DOWN given the DOWN direction", () => {
      const progressiveDirections = AsciiPathFinderUtils.getProgressiveDirections(DIRECTION.DOWN);
      expect(progressiveDirections).to.have.members([DIRECTION.LEFT, DIRECTION.RIGHT, DIRECTION.DOWN]);
    });

    it("Should return LEFT, RIGHT, UP given the UP direction", () => {
      const progressiveDirections = AsciiPathFinderUtils.getProgressiveDirections(DIRECTION.UP);
      expect(progressiveDirections).to.have.members([DIRECTION.LEFT, DIRECTION.RIGHT, DIRECTION.UP]);
    });

    it("Should return LEFT, DOWN, UP given the LEFT direction", () => {
      const progressiveDirections = AsciiPathFinderUtils.getProgressiveDirections(DIRECTION.LEFT);
      expect(progressiveDirections).to.have.members([DIRECTION.LEFT, DIRECTION.DOWN, DIRECTION.UP]);
    });

    it("Should return RIGHT, DOWN, UP given the RIGHT direction", () => {
      const progressiveDirections = AsciiPathFinderUtils.getProgressiveDirections(DIRECTION.RIGHT);
      expect(progressiveDirections).to.have.members([DIRECTION.RIGHT, DIRECTION.DOWN, DIRECTION.UP]);
    });
  });

  describe("isChangingDirection", () => {
    it("Should return true if the direction is changing", () => {
      const b = AsciiPathFinderUtils.isChangingDirection(DIRECTION.DOWN, DIRECTION.LEFT);
      expect(b).to.be.true;
    });

    it("Should return fales if the direction is NOT changing", () => {
      const b = AsciiPathFinderUtils.isChangingDirection(DIRECTION.DOWN, DIRECTION.DOWN);
      expect(b).to.be.false;
    });
  });

  describe("isCornerElement", () => {
    it("Should return true if it is a corner symbol", () => {
      const b = AsciiPathFinderUtils.isCornerSymbol(SYMBOL.CORNER);
      expect(b).to.be.true;
    });

    it("Should return false if it is NOT a corner symbol", () => {
      const b = AsciiPathFinderUtils.isCornerSymbol(SYMBOL.END);
      expect(b).to.be.false;
    });
  });

  describe("isEmptySpace", () => {
    it("Should return true if it is a space character", () => {
      const b = AsciiPathFinderUtils.isEmptySpace(" ");
      expect(b).to.be.true;
    });

    it("Should return false if it is NOT a space character", () => {
      const b = AsciiPathFinderUtils.isEmptySpace(SYMBOL.CORNER);
      expect(b).to.be.false;
    });
  });

  describe("isTFork", () => {
    it("Should return true if it is a horizontal T-fork", () => {
      const b = AsciiPathFinderUtils.isTFork([DIRECTION.LEFT, DIRECTION.RIGHT]);
      expect(b).to.be.true;
    });

    it("Should return true if it is a vertical T-fork", () => {
      const b = AsciiPathFinderUtils.isTFork([DIRECTION.UP, DIRECTION.DOWN]);
      expect(b).to.be.true;
    });

    it("Should return false if there are less then 2 directions", () => {
      const b = AsciiPathFinderUtils.isTFork([DIRECTION.LEFT]);
      expect(b).to.be.false;
    });

    it("Should return false if there are more then 2 directions", () => {
      const b = AsciiPathFinderUtils.isTFork([DIRECTION.LEFT, DIRECTION.RIGHT, DIRECTION.UP]);
      expect(b).to.be.false;
    });

    it("Should return false if it is NOT a T-fork", () => {
      const b = AsciiPathFinderUtils.isTFork([DIRECTION.UP, DIRECTION.RIGHT]);
      expect(b).to.be.false;
    });
  });

  describe("isValidCapitalLetter", () => {
    it("Should return true if it is a valid capital letter", () => {
      const b = AsciiPathFinderUtils.isValidCapitalLetter("A");
      expect(b).to.be.true;
    });

    it("Should return false if it is NOT a valid capital letter", () => {
      const b = AsciiPathFinderUtils.isValidCapitalLetter("Y");
      expect(b).to.be.false;
    });
  });

  describe("isValidSymbol", () => {
    it("Should return true if it is a valid symbol", () => {
      const b = AsciiPathFinderUtils.isValidSymbol(SYMBOL.CORNER);
      expect(b).to.be.true;
    });

    it("Should return false if it is NOT a valid symbol", () => {
      const b = AsciiPathFinderUtils.isValidSymbol("Y");
      expect(b).to.be.false;
    });
  });

  describe("isWithinMatrixBoundaries", () => {
    it("Should return true if the position is within matrix boundries", () => {
      const b = AsciiPathFinderUtils.isWithinMatrixBoundaries({ x: 1, y: 1 }, asciiMatrix);
      expect(b).to.be.true;
    });

    it("Should return false if the position is NOT within matrix boundries", () => {
      const b = AsciiPathFinderUtils.isWithinMatrixBoundaries({ x: 1, y: 2 }, asciiMatrix);
      expect(b).to.be.false;
    });
  });
});
