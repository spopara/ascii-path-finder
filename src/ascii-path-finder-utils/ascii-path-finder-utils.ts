import { DIRECTION, SYMBOL } from "../common/enums";
import { AsciiElement, Position, Step } from "../common/types";

/**
 * Helper methods.
 */
export class AsciiPathFinderUtils {
  static getAsciiElement(position: Position, asciiMatrix: AsciiElement[][]): AsciiElement {
    return asciiMatrix[position.x][position.y];
  }

  static getNextPosition(direction: DIRECTION, currentPosition: Position): Position {
    let nextPosition;
    switch (direction) {
      case DIRECTION.DOWN:
        nextPosition = {
          x: currentPosition.x + 1,
          y: currentPosition.y,
        };
        break;
      case DIRECTION.UP:
        nextPosition = {
          x: currentPosition.x - 1,
          y: currentPosition.y,
        };
        break;
      case DIRECTION.LEFT:
        nextPosition = {
          x: currentPosition.x,
          y: currentPosition.y - 1,
        };
        break;
      case DIRECTION.RIGHT:
        nextPosition = {
          x: currentPosition.x,
          y: currentPosition.y + 1,
        };
        break;
    }

    return nextPosition;
  }

  /**
   * Returns directions that, given the current direction, will keep moving forward.
   * Basicaly returns all directions except reverse direction.
   * Current direction is always the first element in the returned list.
   *
   * @param currentDirection - the current direction.
   * @returns directions that will keep the progress.
   */
  static getProgressiveDirections(currentDirection: DIRECTION | null): DIRECTION[] {
    switch (currentDirection) {
      case DIRECTION.DOWN:
        return [DIRECTION.DOWN, DIRECTION.LEFT, DIRECTION.RIGHT];
      case DIRECTION.UP:
        return [DIRECTION.UP, DIRECTION.LEFT, DIRECTION.RIGHT];
      case DIRECTION.LEFT:
        return [DIRECTION.LEFT, DIRECTION.UP, DIRECTION.DOWN];
      case DIRECTION.RIGHT:
        return [DIRECTION.RIGHT, DIRECTION.UP, DIRECTION.DOWN];
      default:
        return [DIRECTION.DOWN, DIRECTION.LEFT, DIRECTION.RIGHT, DIRECTION.UP];
    }
  }

  static isValidCapitalLetter(character: string): boolean {
    return !!character.match(/[A,B,C,D,E,G,H,I,L,N,O,S]/);
  }

  static isCornerSymbol(character: string): boolean {
    return character === SYMBOL.CORNER;
  }

  static isEmptySpace(character: string): boolean {
    return character === " ";
  }

  static isTFork(directions: DIRECTION[]): boolean {
    if (directions.length !== 2) {
      return false;
    }

    if (directions.includes(DIRECTION.UP) && directions.includes(DIRECTION.DOWN)) {
      return true;
    }

    if (directions.includes(DIRECTION.LEFT) && directions.includes(DIRECTION.RIGHT)) {
      return true;
    }

    return false;
  }

  static isChangingDirection(nextDirection: DIRECTION, currentDirection: DIRECTION | null): boolean {
    return nextDirection !== currentDirection;
  }

  static isValidSymbol(character: string): boolean {
    const validSymbols: string[] = Object.values(SYMBOL);

    return validSymbols.includes(character);
  }

  static isWithinMatrixBoundaries(position: Position, asciiMatrix: AsciiElement[][]): boolean {
    if (position.x < 0) {
      return false;
    }

    if (position.y < 0) {
      return false;
    }

    if (position.x > asciiMatrix.length - 1) {
      return false;
    }

    if (position.y > asciiMatrix[position.x].length - 1) {
      return false;
    }

    return true;
  }
}
