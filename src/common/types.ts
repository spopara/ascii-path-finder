import { DIRECTION } from "./enums";

export interface AsciiElement {
  value: string;
  processed: boolean;
}

export interface Position {
  x: number;
  y: number;
}

export interface Step {
  direction: DIRECTION;
  asciiElement: AsciiElement;
  position: Position;
}
