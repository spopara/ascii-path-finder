export enum DIRECTION {
  UP = "up",
  DOWN = "down",
  LEFT = "left",
  RIGHT = "right",
}

export enum ERROR_MESSAGE {
  BROKEN_PATH = "The path is broken!",
  T_FORK = "The path has a T fork!",
  MULTIPLE_START = "Ascii map has more than one start symbol!",
  MULTIPLE_END = "Ascii map has more than one end symbol!",
  NO_START = "Ascii map has no start symbol!",
  NO_END = "Ascii map has no end symbol!",
  INVALID_CHARACTER = "Invalid character!",
}

export enum SYMBOL {
  START = "@",
  END = "x",
  CORNER = "+",
  LINE_HORIZONTAL = "-",
  LINE_VERTICAL = "|",
}
