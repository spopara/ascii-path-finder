import fs from "fs";
import { AsciiElement } from "../common/types";

/**
 * Parses an ASCII map file into an ASCII matrix.
 */
export class AsciiFileReader {
  private asciiMatrix: AsciiElement[][];

  constructor(filePath: string) {
    const lines = this.readAsciiMapFile(filePath);
    this.asciiMatrix = this.generateAsciiMatrix(lines);
  }

  getAsciiMatrix(): AsciiElement[][] {
    return this.asciiMatrix;
  }

  /**
   * Generates an ASCII matrix based on lines of text.
   * Each line represents a row in the matrix.
   * Each character in the line represents a column in the matrix.
   * The resulting matrix can be jagged (row lenghts of different sizes).
   *
   * @param lines - list of text lines.
   * @returns a jagged ASCII matrix.
   */
  private generateAsciiMatrix(lines: string[]): AsciiElement[][] {
    const asciiMatrix: AsciiElement[][] = [];
    lines.forEach((line: string) => {
      const asciiElements: AsciiElement[] = line.split("").map((char) => {
        const asciiElement: AsciiElement = { value: char, processed: false };
        return asciiElement;
      });
      asciiMatrix.push(asciiElements);
    });

    return asciiMatrix;
  }

  private readAsciiMapFile(filePath: string): string[] {
    const lines = fs.readFileSync(filePath, "utf-8").split(/\r?\n/);

    return lines;
  }
}
