import { AsciiFileReader } from "./ascii-file-reader/ascii-file-reader";
import { AsciiPathFinder } from "./ascii-path-finder/ascii-path-finder";
const yargs = require("yargs");

const argv = yargs
  .option("file", {
    alias: "f",
    description: "ASCII map file path. (mandatory)",
    type: "text",
  })
  .help()
  .alias("help", "h").argv;

if (!argv.file) {
  console.info('File path not specified!');
  process.exit(1);
}

const asciiFileReader = new AsciiFileReader(argv.file);
try {
  const asciiMatrix = asciiFileReader.getAsciiMatrix();
  const asciiPathFinder = new AsciiPathFinder(asciiMatrix);
  console.info(`Letters ${asciiPathFinder.getLetters().join("")}`);
  console.info(`Path as characters ${asciiPathFinder.getPath().join("")}`);
} catch (e) {
  console.info("Error");
}
