const expect = require("chai").expect;
const { promisify } = require("util");
const exec = promisify(require("child_process").exec);

describe("Acceptance Tests", () => {
  describe("Scenario (Valid Maps)", () => {
    it("Should find path in basic example", async () => {
      const result = await exec('node ./build -f "./ascii-maps/valid/basic.txt"');
      expect(result.stdout).to.equal("Letters ACB\nPath as characters @---A---+|C|+---+|+-B-x\n");
    });

    it("Should go straight through intersections", async () => {
      const result = await exec('node ./build -f "./ascii-maps/valid/intersections.txt"');
      expect(result.stdout).to.equal("Letters ABCD\nPath as characters @|A+---B--+|+--C-+|-||+---D--+|x\n");
    });

    it("Should handle letters on turns", async () => {
      const result = await exec('node ./build -f "./ascii-maps/valid/letters-on-turns.txt"');
      expect(result.stdout).to.equal("Letters ACB\nPath as characters @---A---+|||C---+|+-B-x\n");
    });

    it("Should skip letters from same location", async () => {
      const result = await exec('node ./build -f "./ascii-maps/valid/same-location.txt"');
      expect(result.stdout).to.equal("Letters GOONIES\nPath as characters @-G-O-+|+-+|O||+-O-N-+|I|+-+|+-I-+|ES|x\n");
    });

    it("Should handle compact maps", async () => {
      const result = await exec('node ./build -f "./ascii-maps/valid/compact.txt"');
      expect(result.stdout).to.equal("Letters BLAH\nPath as characters @B+++B|+-L-+A+++A-+Hx\n");
    });
  });

  describe("Scenario (Invalid Maps)", () => {
    it("Should return Error if there is no start symbol", async () => {
      const result = await exec('node ./build -f "./ascii-maps/invalid/no-start.txt"');
      expect(result.stdout).to.equal("Error\n");
    });

    it("Should return Error if there is no end symbol", async () => {
      const result = await exec('node ./build -f "./ascii-maps/invalid/no-end.txt"');
      expect(result.stdout).to.equal("Error\n");
    });

    it("Should return Error if there are multiple starts", async () => {
      const result = await exec('node ./build -f "./ascii-maps/invalid/multiple-starts.txt"');
      expect(result.stdout).to.equal("Error\n");
    });

    it("Should return Error if there are multiple ends", async () => {
      const result = await exec('node ./build -f "./ascii-maps/invalid/multiple-ends.txt"');
      expect(result.stdout).to.equal("Error\n");
    });

    it("Should return Error if there is a T-fork", async () => {
      const result = await exec('node ./build -f "./ascii-maps/invalid/t-forks.txt"');
      expect(result.stdout).to.equal("Error\n");
    });

    it("Should return Error if the path is broken", async () => {
      const result = await exec('node ./build -f "./ascii-maps/invalid/broken-path.txt"');
      expect(result.stdout).to.equal("Error\n");
    });

    it("Should return Error if there are multiple starting paths", async () => {
      const result = await exec('node ./build -f "./ascii-maps/invalid/multiple-starting-paths.txt"');
      expect(result.stdout).to.equal("Error\n");
    });

    it("Should return Error if there is a fake turn", async () => {
      const result = await exec('node ./build -f "./ascii-maps/invalid/fake-turn.txt"');
      expect(result.stdout).to.equal("Error\n");
    });
  });
});
